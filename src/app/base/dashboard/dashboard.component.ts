import { Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/services/api/api.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  geoDetails: any = {
    ip: "",
    lat: "",
    lon: ""
  };

  weather: number = 0;
  city: string = "";
  state: string = "";
  datetime: Date;

  weatherIcon: string = "";

  forecasts: any;
  forecastDatetime: number = 0;

  constructor(private api: ApiService) {}

  ngOnInit() {
    this.getIp();
  }

  getIp() {
    this.api.getIp().subscribe(
      res => {
        console.log("SUCCESS: Get IP -> ", res);
        this.geoDetails.ip = res.ip;
        this.geoDetails.lat = res.latitude;
        this.geoDetails.lon = res.longitude;
        this.city = res.city;
        this.state = res.state_prov;
        this.getWeather(this.geoDetails.lat, this.geoDetails.lon);
        this.getForecast(this.geoDetails.lat, this.geoDetails.lon);
      },
      err => {
        console.log("ERROR: Get IP -> ", err);
      }
    );
  }

  getWeather(lat, lon) {
    this.api.getWeather(lat, lon).subscribe(
      res => {
        console.log("SUCCESS: Get Weather Details -> ", res);
        this.weather = parseFloat(res.main.temp) - 273.15;
        this.datetime = new Date(res.dt * 1000);
        this.weatherIcon = "http://openweathermap.org/img/w/" + res.weather[0].icon + ".png";
      },
      err => {
        console.log("ERROR: Get Weather Details -> ", err);
      }
    );
  }

  getForecast(lat, lon) {
    this.api.getForecast(lat, lon).subscribe(
      res => {
        console.log("SUCCESS: Get Forecast Details -> ", res.list);
        this.forecasts = res.list;
      },
      err => {
        console.log("ERROR: Get Forecast Details -> ", err);
      }
    );
  }
}
