import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  openWeatherKey: string = "7fdc5c70994288818c2731f0fc14a357";
  ipGeoLocationKey: string = "a8ef35a3db5d4c139d95d7206308f7b1";

  constructor(private http: HttpClient) {}

  getIp() {
    return this.http.get<any>(`https://api.ipgeolocation.io/ipgeo?apiKey=${this.ipGeoLocationKey}`);
  }

  getWeather(lat, lon) {
    return this.http.get<any>(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${
        this.openWeatherKey
      }`
    );
  }

  getForecast(lat, lon) {
    return this.http.get<any>(
      `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${
        this.openWeatherKey
      }`
    );
  }
}
